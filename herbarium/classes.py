from dataclasses import dataclass

import pandas as pd


@dataclass
class ClassInfo:
    num_families: int
    num_genuses: int
    num_categories: int

    @classmethod
    def from_df(cls, df: pd.DataFrame) -> 'ClassInfo':
        return cls(
            num_families=len(df.family.unique()),
            num_genuses=len(df.genus.unique()),
            num_categories=len(df.category_id.unique()),
        )
