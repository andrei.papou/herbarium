from typing import Tuple

import torch


HierarchicalY = Tuple[torch.Tensor, torch.Tensor, torch.Tensor]
