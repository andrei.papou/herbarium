from pathlib import Path
from typing import Tuple, Optional, Dict

import numpy as np
import pandas as pd
import torch
from albumentations import DualTransform
from PIL import Image
from torch.utils.data import Dataset


class BaseDataset(Dataset):

    def __init__(self, df: pd.DataFrame, image_folder_path: Path, transform: Optional[DualTransform] = None):
        self._df = df
        self._image_folder_path = image_folder_path
        self._transform = transform

    def __len__(self) -> int:
        return len(self._df)


class TrainHierarchicalDataset(BaseDataset):

    def __init__(
            self,
            df: pd.DataFrame,
            image_folder_path: Path,
            fam_to_gen_map: Dict[int, Dict[int, int]],
            gen_to_cat_map: Dict[int, Dict[int, int]],
            transform: Optional[DualTransform] = None):
        super().__init__(df=df, image_folder_path=image_folder_path, transform=transform)
        self._fam_to_gen_map = fam_to_gen_map
        self._gen_to_cat_map = gen_to_cat_map

    def __getitem__(self, item: int) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        row = self._df.iloc[item]
        img = np.array(Image.open(self._image_folder_path / row.file_name)).astype('float32')
        img /= 255.0
        if self._transform:
            img = self._transform(image=img)['image']
        img = torch.from_numpy(img.transpose(2, 0, 1))
        family_target = torch.tensor(row.family).long()
        genus_target = torch.tensor(self._fam_to_gen_map[row.family][row.genus]).long()
        category_target = torch.tensor(self._gen_to_cat_map[row.genus][row.category_id]).long()
        return img, family_target, genus_target, category_target


class TrainFlatDataset(BaseDataset):

    def __len__(self) -> int:
        return len(self._df)

    def __getitem__(self, item: int) -> Tuple[torch.Tensor, torch.Tensor]:
        row = self._df.iloc[item]
        img = np.array(Image.open(self._image_folder_path / row.file_name)).astype('float32')
        img /= 255.0
        if self._transform:
            img = self._transform(image=img)['image']
        img = torch.from_numpy(img.transpose(2, 0, 1))
        target = torch.tensor(row.category_id).long()
        return img, target
