from pathlib import Path
from typing import List, Optional

import numpy as np
import torch
import torch.nn.functional as torch_f
from torch.optim import Adam
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
from tqdm.notebook import tqdm

from herbarium.metrics import MetricReporter
from herbarium.types import HierarchicalY


def get_correctly_predicted(target: torch.Tensor, pred: torch.Tensor) -> int:
    return (torch_f.softmax(pred, dim=1).argmax(dim=1) == target).int().sum().item()


def get_correctly_predicted_hierarchical(target: HierarchicalY, pred: HierarchicalY) -> int:
    family_target, genus_target, category_target = target
    family_pred, genus_pred, category_pred = pred

    family_mask = (torch_f.softmax(family_pred, dim=1).argmax(dim=1) == family_target)
    genus_mask = (torch_f.softmax(genus_pred, dim=1).argmax(dim=1) == genus_target)
    category_mask = (torch_f.softmax(category_pred, dim=1).argmax(dim=1) == category_target)

    return (family_mask & genus_mask & category_mask).int().sum().item()


def to_cuda(*tensor_list: torch.Tensor) -> List[torch.Tensor]:
    return [t.cuda() for t in tensor_list]


def get_hierarchical_loss(target: HierarchicalY, pred: HierarchicalY) -> torch.Tensor:
    family_target, genus_target, category_target = target
    family_pred, genus_pred, category_pred = pred

    family_loss = torch_f.cross_entropy(input=family_pred, target=family_target)
    genus_loss = torch_f.cross_entropy(input=genus_pred, target=genus_target)
    category_loss = torch_f.cross_entropy(input=category_pred, target=category_target)

    return family_loss + genus_loss + category_loss


def train_hierarchical(
        model: torch.nn.Module,
        train_data_loader: DataLoader,
        valid_data_loader: DataLoader,
        epochs: int,
        save_best_to: Path,
        gpu: bool = True,
        lr: float = 1e-3,
        use_tqdm: bool = True,
        metric_reporters: Optional[List[MetricReporter]] = None):
    opt = Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer=opt, min_lr=1e-6)
    metric_reporters = metric_reporters if metric_reporters is not None else []

    for epoch in range(1, epochs + 1):
        opt.zero_grad()
        best_valid_accuracy = 0.0
        train_correct, valid_correct = 0, 0
        train_losses, valid_losses = [], []

        for img, *target in (tqdm(train_data_loader) if use_tqdm else train_data_loader):
            if gpu:
                img, *target = to_cuda(img, *target)
            pred = model(img)
            loss = get_hierarchical_loss(target, pred)

            loss.backward()
            opt.step()
            opt.zero_grad()

            with torch.no_grad():
                train_losses.append(loss.item())
                train_correct += get_correctly_predicted_hierarchical(target, pred)

        train_mean_loss = np.array(train_losses).mean()
        train_accuracy = train_correct / len(train_data_loader.dataset)

        for metric_reporter in metric_reporters:
            metric_reporter.report(epoch=epoch, name='train_loss', value=train_mean_loss)
            metric_reporter.report(epoch=epoch, name='train_accuracy', value=train_accuracy)

        with torch.no_grad():
            for img, *target in (tqdm(valid_data_loader) if use_tqdm else valid_data_loader):
                if gpu:
                    img, *target = to_cuda(img, *target)
                pred = model(img)
                loss = get_hierarchical_loss(target, pred)
                valid_losses.append(loss.item())
                valid_correct += get_correctly_predicted_hierarchical(target, pred)

            valid_mean_loss = np.array(valid_losses).mean()
            valid_accuracy = valid_correct / len(valid_data_loader.dataset)

        for metric_reporter in metric_reporters:
            metric_reporter.report(epoch=epoch, name='valid_loss', value=valid_mean_loss)
            metric_reporter.report(epoch=epoch, name='valid_accuracy', value=valid_accuracy)

        if valid_accuracy > best_valid_accuracy:
            best_valid_accuracy = valid_accuracy
            print(f'Saving best model to {save_best_to}, best accuracy: {best_valid_accuracy}')
            torch.save(model.state_dict(), save_best_to)

        lr_scheduler.step(valid_mean_loss)


def train_flat(
        model: torch.nn.Module,
        train_data_loader: DataLoader,
        valid_data_loader: DataLoader,
        epochs: int,
        save_best_to: Path,
        gpu: bool = True,
        lr: float = 1e-3,
        use_tqdm: bool = True,
        metric_reporters: Optional[List[MetricReporter]] = None):
    opt = Adam(model.parameters(), lr=lr)
    lr_scheduler = ReduceLROnPlateau(optimizer=opt, min_lr=1e-6)
    metric_reporters = metric_reporters if metric_reporters is not None else []

    for epoch in range(1, epochs + 1):
        opt.zero_grad()
        best_valid_accuracy = 0.0
        train_correct, valid_correct = 0, 0
        train_losses, valid_losses = [], []

        for img, target in (tqdm(train_data_loader) if use_tqdm else train_data_loader):
            if gpu:
                img, target = to_cuda(img, target)
            pred = model(img)
            loss = torch_f.cross_entropy(pred, target)

            loss.backward()
            opt.step()
            opt.zero_grad()

            with torch.no_grad():
                train_losses.append(loss.item())
                train_correct += get_correctly_predicted(target, pred)

        train_mean_loss = np.array(train_losses).mean()
        train_accuracy = train_correct / len(train_data_loader.dataset)

        for metric_reporter in metric_reporters:
            metric_reporter.report(epoch=epoch, name='train_loss', value=train_mean_loss)
            metric_reporter.report(epoch=epoch, name='train_accuracy', value=train_accuracy)

        with torch.no_grad():
            for img, target in (tqdm(valid_data_loader) if use_tqdm else valid_data_loader):
                if gpu:
                    img, target = to_cuda(img, target)
                pred = model(img)
                loss = torch_f.cross_entropy(pred, target)
                valid_losses.append(loss.item())
                valid_correct += get_correctly_predicted(target, pred)

            valid_mean_loss = np.array(valid_losses).mean()
            valid_accuracy = valid_correct / len(valid_data_loader.dataset)

        for metric_reporter in metric_reporters:
            metric_reporter.report(epoch=epoch, name='valid_loss', value=valid_mean_loss)
            metric_reporter.report(epoch=epoch, name='valid_accuracy', value=valid_accuracy)

        if valid_accuracy > best_valid_accuracy:
            best_valid_accuracy = valid_accuracy
            print(f'Saving best model to {save_best_to}, best accuracy: {best_valid_accuracy}')
            torch.save(model.state_dict(), save_best_to)

        lr_scheduler.step(valid_mean_loss)
