from dataclasses import dataclass
from typing import Dict

import pandas as pd


@dataclass
class HierarchyClassInfo:
    family_out_size: int
    genus_out_size: int
    category_out_size: int

    @classmethod
    def from_df(cls, df: pd.DataFrame) -> 'HierarchyClassInfo':
        unique_families = df.family.unique()
        family_out_size = len(unique_families)
        genus_out_size = max([len(df[df.family == family].genus.unique()) for family in unique_families])
        category_out_size = max([len(df[df.genus == genus].category_id.unique()) for genus in df.genus.unique()])
        return cls(
            family_out_size=family_out_size,
            genus_out_size=genus_out_size,
            category_out_size=category_out_size
        )


def build_fam_to_gen_map(df: pd.DataFrame) -> Dict[int, Dict[int, int]]:
    return {
        family_id: {
            genus_id: genus_pos
            for genus_pos, genus_id in enumerate(sorted(df[df.family == family_id].genus.unique()))
        } for family_id in df.family.unique()
    }


def build_gen_to_cat_map(df: pd.DataFrame) -> Dict[int, Dict[int, int]]:
    return {
        genus_id: {
            category_id: category_pos
            for category_pos, category_id in enumerate(sorted(df[df.genus == genus_id].category_id.unique()))
        } for genus_id in df.genus.unique()
    }
