import setuptools


with open('requirements.txt') as f:
    requirements = f.readlines()


setuptools.setup(
    name='herbarium',
    version='0.0.1',
    author='Andrei Papou',
    author_email='popow.andrej2009@yandex.ru',
    description='Herbarium Kaggle contest utilities',
    long_description='',
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    install_requires=requirements,
    classifiers=[
        'Programming Language :: Python :: 3.7',
        'Operating System :: OS Independent',
    ]
)
