import json
import os
import sys
from typing import Dict, Optional


def authenticate(credentials: Dict[str, str]):
    os.system(
        'mkdir -p ~/.kaggle &&'
        f'echo \'{json.dumps(credentials)}\' > ~/.kaggle/kaggle.json &&'
        'chmod 600 ~/.kaggle/kaggle.json'
    )


def download_data(dst_path: str, auth_with: Optional[Dict[str, str]]):
    if auth_with is not None:
        authenticate(auth_with)
    os.system(
        f'mkdir -p ~/.kaggle &&'
        f'{sys.executable} kaggle datasets download andreippv/herbarium-2020-256x256 &&'
        f'unzip -qq herbarium-2020-256x256.zip -d {dst_path} &&'
        f'rm herbarium-2020-256x256.zip'
    )
