import abc

from torch.utils.tensorboard import SummaryWriter


class MetricReporter(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def report(self, epoch: int, name: str, value: float):
        raise NotImplementedError()


class StdoutMetricReporter(MetricReporter):

    def report(self, epoch: int, name: str, value: float):
        print(f'[{epoch}] {name} = {value}')


class TensorBoardMetricReporter(MetricReporter):

    def __init__(self):
        self._writer = SummaryWriter()

    def report(self, epoch: int, name: str, value: float):
        self._writer.add_scalar(tag=name, scalar_value=value, global_step=epoch)
