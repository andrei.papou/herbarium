import torch
import torch.nn.functional as torch_f

from herbarium.classes import ClassInfo
from herbarium.hierarchy import HierarchyClassInfo
from herbarium.types import HierarchicalY


class Backbone:

    def __init__(self, module: torch.nn.Module, fc_attr: str = 'fc'):
        self.module = module
        fc = getattr(module, fc_attr, None)
        if not isinstance(fc, torch.nn.Linear):
            raise ValueError(f'{fc} should be instance of torch.nn.Linear.')
        self.fc = fc


class HierarchicalModel(torch.nn.Module):

    def __init__(self, backbone: Backbone, hierarchy_class_info: HierarchyClassInfo):
        super().__init__()
        self._backbone = backbone.module
        self._family_fc = torch.nn.Linear(backbone.fc.out_features, hierarchy_class_info.family_out_size)
        self._genus_fc = torch.nn.Linear(backbone.fc.out_features, hierarchy_class_info.genus_out_size)
        self._category_fc = torch.nn.Linear(backbone.fc.out_features, hierarchy_class_info.category_out_size)

    def forward(self, x: torch.Tensor) -> HierarchicalY:
        x = torch_f.relu(self._backbone(x))
        family_pred = self._family_fc(x)
        genus_pred = self._genus_fc(x)
        category_pred = self._category_fc(x)
        return family_pred, genus_pred, category_pred


class HierarchicalSequentialModel(torch.nn.Module):

    def __init__(self, backbone: Backbone, hierarchy_class_info: HierarchyClassInfo):
        super().__init__()
        self._backbone = backbone.module
        self._family_fc = torch.nn.Linear(backbone.fc.out_features, hierarchy_class_info.family_out_size)
        self._genus_fc = torch.nn.Linear(
            backbone.fc.out_features + self._family_fc.out_features,
            hierarchy_class_info.genus_out_size)
        self._category_fc = torch.nn.Linear(
            backbone.fc.out_features + self._family_fc.out_features + self._genus_fc.out_features,
            hierarchy_class_info.category_out_size)

    def forward(self, x: torch.Tensor) -> HierarchicalY:
        x = torch_f.relu(self._backbone(x))
        family_pred = self._family_fc(x)
        genus_pred = self._genus_fc(torch_f.relu(torch.cat([x, family_pred], dim=1)))
        category_pred = self._category_fc(torch_f.relu(torch.cat([x, family_pred, genus_pred], dim=1)))
        return family_pred, genus_pred, category_pred


class FlatModel(torch.nn.Module):

    def __init__(self, backbone: Backbone, class_info: ClassInfo):
        super().__init__()
        self._backbone = backbone.module
        self._fc = torch.nn.Linear(backbone.fc.out_features, class_info.num_categories)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = torch_f.relu(self._backbone(x))
        return self._fc(x)

